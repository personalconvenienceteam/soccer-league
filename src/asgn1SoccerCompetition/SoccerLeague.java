package asgn1SoccerCompetition;

import java.util.ArrayList;
import java.util.Collections;

import asgn1Exceptions.LeagueException;
import asgn1Exceptions.TeamException;

/**
 * A class to model a soccer league. Matches are played between teams and points
 * awarded for a win, loss or draw. After each match teams are ranked, first by
 * points, then by goal difference and then alphabetically.
 * 
 * @author Alan Woodley
 * @version 1.0
 *
 */
public class SoccerLeague implements SportsLeague {
    // Specifies the number of team required/limit of teams for the league
    private int requiredTeams;
    // Specifies is the league is in the off season
    private boolean offSeason;

    private ArrayList<SoccerTeam> soccerLeague;

    /**
     * Generates a model of a soccer team with the specified number of teams. A
     * season can not start until that specific number of teams has been added.
     * Once that number of teams has been reached no more teams can be added
     * unless a team is first removed.
     * 
     * @param requiredTeams
     *            The number of teams required/limit for the league.
     */
    public SoccerLeague(int requiredTeams) {
        this.requiredTeams = requiredTeams;
        this.offSeason = true;
        soccerLeague = new ArrayList<SoccerTeam>(requiredTeams);
    }

    /**
     * Registers a team to the league.
     * 
     * @param team
     *            Registers a team to play in the league.
     * @throws LeagueException
     *             If the season has already started, if the maximum number of
     *             teams allowed to register has already been reached or a team
     *             with the same official name has already been registered.
     */
    public void registerTeam(SoccerTeam team) throws LeagueException {
        if (!offSeason)
            throw new LeagueException("Cannot add team to seasons in progress");

        if (this.soccerLeague.size() >= this.requiredTeams)
            throw new LeagueException("Cannot add new team, League Full");

        if (containsTeam(team.getOfficialName()))
            throw new LeagueException("Team with that name already exists in league");

        this.soccerLeague.add(team);
    }

    /**
     * Removes a team from the league.
     * 
     * @param team
     *            The team to remove
     * @throws LeagueException
     *             if the season has not ended or if the team is not registered
     *             into the league.
     */
    public void removeTeam(SoccerTeam team) throws LeagueException {
        if (!offSeason)
            throw new LeagueException("Cannot remove teams while season is in progress");

        if (!containsTeam(team.getOfficialName()))
            throw new LeagueException("Team does not exist in league");

        this.soccerLeague.remove(team);
    }

    /**
     * Gets the number of teams currently registered to the league
     * 
     * @return the current number of teams registered
     */
    public int getRegisteredNumTeams() {
        return this.soccerLeague.size();
    }

    /**
     * Gets the number of teams required for the league to begin its season
     * which is also the maximum number of teams that can be registered to a
     * league.
     * 
     * @return The number of teams required by the league/maximum number of
     *         teams in the league
     */
    public int getRequiredNumTeams() {
        return requiredTeams;
    }

    /**
     * Starts a new season by reverting all statistics for each team to initial
     * values.
     * 
     * @throws LeagueException
     *             if the number of registered teams does not equal the required
     *             number of teams or if the season has already started
     */
    public void startNewSeason() throws LeagueException {
        if (this.requiredTeams > this.soccerLeague.size())
            throw new LeagueException("League does not have required number of teams to start season");

        if (!this.offSeason)
            throw new LeagueException("Season has already started");

        for (SoccerTeam team : this.soccerLeague) {
            team.resetStats();
        }

        this.offSeason = false;

    }

    /**
     * Ends the season.
     * 
     * @throws LeagueException
     *             if season has not started
     */
    public void endSeason() throws LeagueException {
        if (this.offSeason)
            throw new LeagueException("Season has not commenced");

        this.offSeason = true;
    }

    /**
     * Specifies if the league is in the off season (i.e. when matches are not
     * played).
     * 
     * @return True If the league is in its off season, false otherwise.
     */
    public boolean isOffSeason() {
        return this.offSeason;
    }

    /**
     * Returns a team with a specific name.
     * 
     * @param name
     *            The official name of the team to search for.
     * @return The team object with the specified official name.
     * @throws LeagueException
     *             if no team has that official name.
     */
    public SoccerTeam getTeamByOfficalName(String name) throws LeagueException {
        for (SoccerTeam team : this.soccerLeague) {
            if (team.getOfficialName() == name) {
                return team;
            }
        }

        throw new LeagueException("Team Does not exist in league");

    }

    /**
     * Plays a match in a specified league between two teams with the respective
     * goals. After each match the teams are resorted.
     *
     * @param homeTeamName
     *            The name of the home team.
     * @param homeTeamGoals
     *            The number of goals scored by the home team.
     * @param awayTeamName
     *            The name of the away team.
     * @param awayTeamGoals
     *            The number of goals scored by the away team.
     * @throws LeagueException
     *             If the season has not started or if both teams have the same
     *             official name.
     */
    public void playMatch(String homeTeamName, int homeTeamGoals, String awayTeamName, int awayTeamGoals)
            throws LeagueException {

        if (this.offSeason)
            throw new LeagueException("Matches cannot be played in off season");

        if (homeTeamName == awayTeamName)
            throw new LeagueException("Error, team cannot play itself");

        SoccerTeam homeTeam = this.getTeamByOfficalName(homeTeamName);
        SoccerTeam awayTeam = this.getTeamByOfficalName(awayTeamName);

        try {
            homeTeam.playMatch(homeTeamGoals, awayTeamGoals);
            awayTeam.playMatch(awayTeamGoals, homeTeamGoals);
        } catch (TeamException exception) {
            System.out.println(exception.getMessage());
            exception.printStackTrace();
        }

        this.sortTeams();

    }

    /**
     * Displays a ranked list of the teams in the league to the screen.
     */
    public void displayLeagueTable() {
        // TO DO (optional)
    }

    /**
     * Returns the highest ranked team in the league.
     *
     * @return The highest ranked team in the league.
     * @throws LeagueException
     *             if the number of teams is zero or less than the required
     *             number of teams.
     */
    public SoccerTeam getTopTeam() throws LeagueException {
        if (this.soccerLeague.size() < this.requiredTeams)
            throw new LeagueException("League is not full");

        if (this.soccerLeague.size() == 0)
            throw new LeagueException("League has zero teams");

        return this.soccerLeague.get(this.requiredTeams - 1);

    }

    /**
     * Returns the lowest ranked team in the league.
     *
     * @return The lowest ranked team in the league.
     * @throws LeagueException
     *             if the number of teams is zero or less than the required
     *             number of teams.
     */
    public SoccerTeam getBottomTeam() throws LeagueException {
        if (this.soccerLeague.size() < this.requiredTeams)
            throw new LeagueException("League is not full");

        if (this.soccerLeague.size() == 0)
            throw new LeagueException("League has zero teams");

        return this.soccerLeague.get(0);
    }

    /**
     * Sorts the teams in the league.
     */
    public void sortTeams() {
        // Create a temporary copy of the league to store teams in during sorting
        SoccerLeague tempLeague = new SoccerLeague(this.requiredTeams);
        for (int index = 0; index < this.requiredTeams; index++) {
            tempLeague.soccerLeague.add(index, this.soccerLeague.get(index));
        }
        
        // Use a merge sort to sort teams
        this.runMergeSortAlgorithm(0, requiredTeams - 1, tempLeague);
    }

    /**
     * This method creates the indices need to perform a merge sort
     * 
     * @param bottomIndex
     * @param topIndex
     * @param tempLeague
     */
    private void runMergeSortAlgorithm(int bottomIndex, int topIndex, SoccerLeague tempLeague) {

        if (bottomIndex < topIndex) {
            int midIndex = bottomIndex + (topIndex - bottomIndex) / 2;
            runMergeSortAlgorithm(bottomIndex, midIndex, tempLeague);
            runMergeSortAlgorithm(midIndex + 1, topIndex, tempLeague);
            mergeSort(bottomIndex, midIndex, topIndex, tempLeague);
        }
    }

    /**
     * This method uses a merge sort algorithm to sort the teams in the league
     * The team at the first index is compared to the team at in the middle.
     * The lower team is then placed at the bottom of the league.
     *  
     * @param bottomIndex
     * @param midIndex
     * @param topIndex
     * @param tempLeague
     */
    private void mergeSort(int bottomIndex, int midIndex, int topIndex, SoccerLeague tempLeague) {

        int firstIndex = bottomIndex;
        int secondIndex = midIndex + 1;
        int thirdIndex = bottomIndex;

        while (firstIndex <= midIndex && secondIndex <= topIndex) {
            SoccerTeam firstTeam = tempLeague.soccerLeague.get(firstIndex);
            SoccerTeam secondTeam = tempLeague.soccerLeague.get(secondIndex);

            if (firstTeam.compareTo(secondTeam) >= 0) {
                this.soccerLeague.set(thirdIndex, tempLeague.soccerLeague.get(firstIndex));
                firstIndex++;
            } else {
                this.soccerLeague.set(thirdIndex, tempLeague.soccerLeague.get(secondIndex));
                secondIndex++;
            }
            thirdIndex++;
        }
        while (firstIndex <= midIndex) {
            this.soccerLeague.set(thirdIndex, tempLeague.soccerLeague.get(firstIndex));
            thirdIndex++;
            firstIndex++;
        }

    }

    /**
     * Specifies if a team with the given official name is registered to the
     * league.
     * 
     * @param name
     *            The name of a team.
     * @return True if the team is registered to the league, false otherwise.
     */
    public boolean containsTeam(String name) {

        for (SoccerTeam team : this.soccerLeague) {
            if (team.getOfficialName() == name) {
                return true;
            }
        }
        return false;
    }

}
