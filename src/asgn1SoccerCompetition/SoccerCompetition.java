/**
 * 
 */
package asgn1SoccerCompetition;

import java.util.ArrayList;

import asgn1Exceptions.CompetitionException;
import asgn1Exceptions.LeagueException;

/**
 * A class to model a soccer competition. The competition contains one or more
 * number of leagues, each of which contain a number of teams. Over the course a
 * season matches are played between teams in each league. At the end of the
 * season a premier (top ranked team) and wooden spooner (bottom ranked team)
 * are declared for each league. If there are multiple leagues then relegation
 * and promotions occur between the leagues.
 * 
 * @author Alan Woodley
 * @version 1.0
 *
 */
public class SoccerCompetition implements SportsCompetition {

    String name;
    private int numLeagues;
    private ArrayList<SoccerLeague> soccerCompetition;

    /**
     * Creates the model for a new soccer competition with a specific name,
     * number of leagues and number of teams in each league
     * 
     * @param name
     *            The name of the competition.
     * @param numLeagues
     *            The number of leagues in the competition.
     * @param numTeams
     *            The number of teams in each league.
     */
    public SoccerCompetition(String name, int numLeagues, int numTeams) {
        this.name = name;
        this.numLeagues = numLeagues;

        soccerCompetition = new ArrayList<SoccerLeague>(numLeagues);

        for (int league = 0; league < numLeagues; league++) {
            soccerCompetition.add(new SoccerLeague(numTeams));
        }
    }

    /**
     * Retrieves a league with a specific number (indexed from 0). Returns an
     * exception if the league number is invalid.
     * 
     * @param leagueNum
     *            The number of the league to return.
     * @return A league specified by leagueNum.
     * @throws CompetitionException
     *             if the specified league number is less than 0. or equal to or
     *             greater than the number of leagues in the competition.
     */
    public SoccerLeague getLeague(int leagueNum) throws CompetitionException {
        // Ensure league number is valid, otherwise throw an exception
        if (leagueNum < 0 || leagueNum > this.soccerCompetition.size())
            throw new CompetitionException("Invalid league number");

        return this.soccerCompetition.get(leagueNum);
    }

    /**
     * Starts a new soccer season for each league in the competition.
     */
    public void startSeason() {

        // Loop through the competition and start each league's season
        for (SoccerLeague league : this.soccerCompetition) {

            try {
                league.startNewSeason();
            } catch (LeagueException exception) {
                System.out.println(exception.getMessage());
                exception.printStackTrace();
            }
        }

    }

    /**
     * Ends the season of each of the leagues in the competition. If there is
     * more than one league then it handles promotion and relegation between the
     * leagues.
     * 
     */
    public void endSeason() {
        // Temporary leagues and teams for shifting teams
        SoccerLeague currentLeague;
        SoccerLeague nextHighestLeague;
        SoccerTeam premiers;
        SoccerTeam woodenSpoon;

        for (int currentLeagueNum = 0; currentLeagueNum < this.numLeagues; currentLeagueNum++) {
            // Field for league above the current league, used when moving teams
            // up or down leagues
            int nextHighestLeagueNum = currentLeagueNum - 1;

            try {
                // Try to retrieve and close current league
                currentLeague = this.getLeague(currentLeagueNum);
                currentLeague.endSeason();

                // If current league isn't the top league, retrieve and store
                // next league up
                if (nextHighestLeagueNum >= 0) {
                    nextHighestLeague = this.getLeague(nextHighestLeagueNum);
                } else {
                    nextHighestLeague = this.getLeague(currentLeagueNum);
                }

                // Starting with the second league in the competition, move
                // premiers up and pull wooden spoons down
                if (currentLeagueNum != 0) {
                    premiers = currentLeague.getTopTeam();
                    woodenSpoon = nextHighestLeague.getBottomTeam();

                    currentLeague.removeTeam(premiers);
                    currentLeague.registerTeam(woodenSpoon);

                    nextHighestLeague.removeTeam(woodenSpoon);
                    nextHighestLeague.registerTeam(premiers);
                }

                // Catch blocks for printing out exceptions
            } catch (CompetitionException exception) {
                System.out.println(exception.getMessage());
                exception.printStackTrace();
            } catch (LeagueException exception) {
                System.out.println(exception.getMessage());
                exception.printStackTrace();
            }
        }
    }

    /**
     * For each league displays the competition standings.
     */
    public void displayCompetitionStandings() {
        System.out.println("+++++" + this.name + "+++++");

        // TO DO (optional)
        // HINT The heading for each league is
        // System.out.println("---- League" + (i +1) + " ----");
        // System.out.println("Official Name" + '\t' + "Nick Name" + '\t' +
        // "Form" + '\t' + "Played" + '\t' + "Won" + '\t' + "Lost" + '\t' +
        // "Drawn" + '\t' + "For" + '\t' + "Against" + '\t' + "GlDiff" + '\t' +
        // "Points");
    }

}
