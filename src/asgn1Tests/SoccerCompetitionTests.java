package asgn1Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn1Exceptions.CompetitionException;
import asgn1Exceptions.LeagueException;
import asgn1Exceptions.TeamException;
import asgn1SoccerCompetition.SoccerCompetition;
import asgn1SoccerCompetition.SoccerLeague;
import asgn1SoccerCompetition.SoccerTeam;

/**
 * A set of JUnit tests for the asgn1SoccerCompetition.SoccerCompetition class
 *
 * @author Alan Woodley
 *
 */
public class SoccerCompetitionTests {
	final int NumberOfTeams = 4; // Number of teams required for league
	final String[] teamNamesLOne = {"TeamOne1", "T11", "TeamTwo1", "T21", "TeamThree1", 
			  "T31", "TeamFour1", "T41"}; 					  //String of Arrays for team names
	final String[] teamNamesLTwo = {"TeamOne2", "T12", "TeamTwo2", "T22", "TeamThree2", 
			  "T32", "TeamFour2", "T42"}; 					  //String of Arrays for team names
	final String[] teamNamesLThree = {"TeamOne3", "T13", "TeamTwo3", "T23", "TeamThree3", 
			  "T33", "TeamFour3", "T43"}; 					  //String of Arrays for team names
	final String[] teamNamesLFour = {"TeamOne4", "T14", "TeamTwo4", "T24", "TeamThree4", 
			  "T34", "TeamFour4", "T44"}; 					  //String of Arrays for team names

	SoccerCompetition testComp;
	/**
	 * Construct competition for testing
	 * @throws TeamException
	 * @throws LeagueException
	 * @throws CompetitionException
	 */
	@Before
	public void constructTestComp() throws TeamException, LeagueException, CompetitionException{
		testComp = new SoccerCompetition("Test Comp", NumberOfTeams, NumberOfTeams);
		
		for(int officialName = 0; officialName <= NumberOfTeams * 2 - 1; officialName +=2){
			int nickName=officialName+1;
			SoccerTeam team = new SoccerTeam(teamNamesLOne[officialName], teamNamesLOne[nickName]);
			testComp.getLeague(0).registerTeam(team);
		}

		for(int officialName = 0; officialName <= NumberOfTeams * 2 - 1; officialName +=2){
			int nickName=officialName+1;
			SoccerTeam team = new SoccerTeam(teamNamesLTwo[officialName], teamNamesLOne[nickName]);
			testComp.getLeague(1).registerTeam(team);
		}

		for(int officialName = 0; officialName <= NumberOfTeams * 2 - 1; officialName +=2){
			int nickName=officialName+1;
			SoccerTeam team = new SoccerTeam(teamNamesLThree[officialName], teamNamesLOne[nickName]);
			testComp.getLeague(2).registerTeam(team);
		}

		for(int officialName = 0; officialName <= NumberOfTeams * 2 - 1; officialName +=2){
			int nickName=officialName+1;
			SoccerTeam team = new SoccerTeam(teamNamesLFour[officialName], teamNamesLOne[nickName]);
			testComp.getLeague(3).registerTeam(team);
		}

	}
	
	/**
	 * test league can be retrieved correctly
	 * @throws CompetitionException
	 */
	@Test
	public void testGetLeague() throws CompetitionException{
		SoccerLeague expectedLeague = testComp.getLeague(0);
		assertEquals(testComp.getLeague(0), expectedLeague);
	}
	
	/**
	 * Test that getLeague() throws an exception when passed a negative value 
	 * @throws CompetitionException
	 */
	@Test (expected = CompetitionException.class)
	public void testGetLeagueExceptionNegative() throws CompetitionException{
		testComp.getLeague(-1);
	}
	
	/**
	 * Test that getLeague() throws and exception when passed a value greater than number of leagues
	 * in the competition
	 * @throws CompetitionException
	 */
	@Test (expected = CompetitionException.class)
	public void testGetLeagueExceptionTooHigh() throws CompetitionException{
		testComp.getLeague(5);
	}
	
	/**
	 * Test startSeason() starts the seasons correctly
	 * @throws CompetitionException
	 */
	@Test
	public void testStartSeason() throws CompetitionException{
		testComp.startSeason();
		assertFalse(testComp.getLeague(0).isOffSeason());
	}
	
	/**
	 * Test the end season ends correctly and moves teams between leagues as specified
	 * @throws CompetitionException
	 * @throws LeagueException
	 */
	@Test
	public void testEndSeason() throws CompetitionException, LeagueException{
		SoccerTeam woodenSpoonLZero = testComp.getLeague(0).getBottomTeam();
		SoccerTeam woodenSpoonLOne = testComp.getLeague(1).getBottomTeam();
		SoccerTeam woodenSpoonLTwo = testComp.getLeague(2).getBottomTeam();
		SoccerTeam premierLOne = testComp.getLeague(1).getTopTeam();
		SoccerTeam premierLTwo = testComp.getLeague(2).getTopTeam();
		SoccerTeam premierLThree = testComp.getLeague(3).getTopTeam();
		
		testComp.startSeason();
		testComp.endSeason();
		assertTrue(testComp.getLeague(0).isOffSeason());
		assertFalse(testComp.getLeague(0).containsTeam(woodenSpoonLZero.getOfficialName()));
		assertTrue(testComp.getLeague(0).containsTeam(premierLOne.getOfficialName()));
		assertTrue(testComp.getLeague(1).containsTeam(premierLTwo.getOfficialName()));
		assertTrue(testComp.getLeague(2).containsTeam(premierLThree.getOfficialName()));
		assertTrue(testComp.getLeague(1).containsTeam(woodenSpoonLZero.getOfficialName()));
		assertTrue(testComp.getLeague(2).containsTeam(woodenSpoonLOne.getOfficialName()));
		assertTrue(testComp.getLeague(3).containsTeam(woodenSpoonLTwo.getOfficialName()));
	}
}
