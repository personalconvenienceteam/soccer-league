package asgn1Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn1Exceptions.LeagueException;
import asgn1Exceptions.TeamException;
import asgn1SoccerCompetition.SoccerLeague;
import asgn1SoccerCompetition.SoccerTeam;


/**
 * A set of JUnit tests for the asgn1SoccerCompetition.SoccerLeage class
 *
 * @author Alan Woodley
 *
 */
public class SoccerLeagueTests {

	final int NumberOfTeams = 4; // Number of teams required for league
	final String[] teamNames = {"TeamOne", "T1", "TeamTwo", "T2", "TeamThree", 
			  "T3", "TeamFour", "T4", "ExceptionTeam", "ET"}; 					  //String of Arrays for team names

	SoccerLeague testLeague; // A sample test league

	//Sample test teams to populate list, exception team used for testing exceptions (i.e. adding to a full league)
	SoccerTeam testTeamOne, testTeamTwo, testTeamThree, testTeamFour, exceptionTeam; 
	
	
	@Before
	public void constructLeagueAndTeams() throws TeamException{
		testLeague = new SoccerLeague(NumberOfTeams);
		testTeamOne = new SoccerTeam(teamNames[0],teamNames[1]);
		testTeamTwo = new SoccerTeam(teamNames[2],teamNames[3]);
		testTeamThree = new SoccerTeam(teamNames[4],teamNames[5]);
		testTeamFour = new SoccerTeam(teamNames[6],teamNames[7]);
		exceptionTeam = new SoccerTeam(teamNames[8],teamNames[9]);
	}
	
	// Test a team can be registered to the league
	@Test
	public void testRegisterTeam() throws LeagueException{
		testLeague.registerTeam(testTeamOne);
		assertTrue(testLeague.containsTeam("TeamOne"));
	}
	
	// Test multiple teams can be added to the league
	@Test
	public void testMultipleTeamsRegistered() throws LeagueException{
		testLeague.registerTeam(testTeamOne);
		testLeague.registerTeam(testTeamTwo);
		testLeague.registerTeam(testTeamThree);
		testLeague.registerTeam(testTeamFour);
		assertTrue(testLeague.containsTeam(teamNames[0]));
		assertTrue(testLeague.containsTeam(teamNames[2]));
		assertTrue(testLeague.containsTeam(teamNames[4]));
		assertTrue(testLeague.containsTeam(teamNames[6]));
	}
	
	// Test exception thrown when attempting to add to many teams to league 
	@Test (expected = LeagueException.class)
	public void testTooManyTeamsException() throws LeagueException{
		testLeague.registerTeam(testTeamOne);
		testLeague.registerTeam(testTeamTwo);
		testLeague.registerTeam(testTeamThree);
		testLeague.registerTeam(testTeamFour);
		testLeague.registerTeam(exceptionTeam);
	}
	
	// Test same team cannot be added multiple times
	@Test (expected = LeagueException.class)
	public void testSameTeamException() throws LeagueException{
		testLeague.registerTeam(testTeamOne);
		testLeague.registerTeam(testTeamOne);
	}

	// Test team cannot be added to in progress season
	@Test (expected = LeagueException.class)
	public void testOffSeasonException() throws LeagueException{
		testLeague.registerTeam(testTeamOne);
		testLeague.registerTeam(testTeamTwo);
		testLeague.registerTeam(testTeamThree);
		testLeague.registerTeam(testTeamFour);
		testLeague.startNewSeason();
		testLeague.registerTeam(exceptionTeam);
	}
	
	// Test team can be removed
	@Test
	public void testTeamRemoval() throws LeagueException{
		testLeague.registerTeam(testTeamOne);
		testLeague.removeTeam(testTeamOne);
		assertFalse(testLeague.containsTeam(teamNames[0]));
	}
	
	// Test removeTeam() throws exception when attempting to remove an invalid team
	@Test (expected = LeagueException.class)
	public void testRemoveExceptionNoTeam() throws LeagueException{
		testLeague.removeTeam(testTeamOne);
	}
	
	// Test removeTeam() throws an exception and prevents teams being removed from an in progress season
	@Test (expected = LeagueException.class)
	public void testRemoveExceptionInSeason() throws LeagueException{
		testLeague.registerTeam(testTeamOne);
		testLeague.registerTeam(testTeamTwo);
		testLeague.registerTeam(testTeamThree);
		testLeague.registerTeam(testTeamFour);
		testLeague.startNewSeason();
		testLeague.removeTeam(testTeamOne);
		assertTrue(testLeague.containsTeam(teamNames[0]));
	}

	// Test getRegisteredNumTeams() returns the expected value 
	@Test
	public void testNumberOfTeamRegistered() throws LeagueException{
		final int expectedresult = 3;
		testLeague.registerTeam(testTeamOne);
		testLeague.registerTeam(testTeamTwo);
		testLeague.registerTeam(testTeamThree);
		assertEquals(testLeague.getRegisteredNumTeams(), expectedresult);
	}
	
	// test getRequiredNumTeams returns the expected value
	@Test
	public void testRequiredTeams(){
		assertEquals(testLeague.getRequiredNumTeams(), NumberOfTeams);
	}
	// test that startNewSeason() throws an exception when attempting to start an empty league
	@Test (expected = LeagueException.class)
	public void testSeasonStartTeamsException() throws LeagueException{
		testLeague.startNewSeason();
	}
	
	// test that startNewSeason() throws an exception when attempting to start an already started season
	@Test (expected = LeagueException.class)
	public void testSeasonStartedException() throws LeagueException{
		testLeague.registerTeam(testTeamOne);
		testLeague.registerTeam(testTeamTwo);
		testLeague.registerTeam(testTeamThree);
		testLeague.registerTeam(testTeamFour);
		testLeague.startNewSeason();
		testLeague.startNewSeason();
	}
	
	// test that endSeason() works as specified
	@Test 
	public void testOffSeasonSwitch() throws LeagueException{
		testLeague.registerTeam(testTeamOne);
		testLeague.registerTeam(testTeamTwo);
		testLeague.registerTeam(testTeamThree);
		testLeague.registerTeam(testTeamFour);
		testLeague.startNewSeason();
		testLeague.endSeason();
		assertTrue(testLeague.isOffSeason());
	}
	
	// test the endSeason() can't end a season that hasn't started
	@Test (expected = LeagueException.class)
	public void testCantEndOffSeason() throws LeagueException{
		testLeague.endSeason();
	}
	
	// test that getTeamByOfficialName works as specified
	@Test
	public void testGetTeamByName() throws LeagueException{
		testLeague.registerTeam(testTeamOne);
		testLeague.registerTeam(testTeamTwo);
		testLeague.registerTeam(testTeamThree);
		testLeague.registerTeam(testTeamFour);
		assertEquals(testLeague.getTeamByOfficalName(teamNames[0]), testTeamOne);
		assertEquals(testLeague.getTeamByOfficalName(teamNames[2]), testTeamTwo);
		assertEquals(testLeague.getTeamByOfficalName(teamNames[4]), testTeamThree);
		assertEquals(testLeague.getTeamByOfficalName(teamNames[6]), testTeamFour);
	}
	
	// test that getTeamByOfficial name throws an exception when passed an invalid string
	@Test (expected = LeagueException.class)
	public void testGetTeamByNameException() throws LeagueException{
		testLeague.registerTeam(testTeamOne);
		testLeague.registerTeam(testTeamTwo);
		testLeague.registerTeam(testTeamThree);
		testLeague.registerTeam(testTeamFour);
		testLeague.getTeamByOfficalName("This String Should Cause An Exception");
	}
	
	// test that playMatches correctly plays and sorts teams
	@Test
	public void testPlayMatchesSort() throws LeagueException{
		testLeague.registerTeam(testTeamOne);
		testLeague.registerTeam(testTeamTwo);
		testLeague.registerTeam(testTeamThree);
		testLeague.registerTeam(testTeamFour);
		testLeague.startNewSeason();
		testLeague.playMatch(teamNames[0], 3, teamNames[2], 2);
		testLeague.playMatch(teamNames[4], 2, teamNames[6], 3);
		testLeague.playMatch(teamNames[0], 1, teamNames[4], 4);
		testLeague.playMatch(teamNames[2], 0, teamNames[6], 5);
		assertEquals(testLeague.getTopTeam(), testTeamFour);
		assertEquals(testLeague.getBottomTeam(), testTeamTwo);
	}
	
	// test that and exception is thrown when attempting to play a match off season
	@Test (expected = LeagueException.class)
	public void testPlayMatchesSeasonException() throws LeagueException{
		testLeague.registerTeam(testTeamOne);
		testLeague.registerTeam(testTeamTwo);
		testLeague.registerTeam(testTeamThree);
		testLeague.registerTeam(testTeamFour);
		testLeague.playMatch(teamNames[0], 3, teamNames[2], 2);
	}

	// Test that an exception is thrown when attempting to have a team play itself
	@Test (expected = LeagueException.class)
	public void testPlayMatchException() throws LeagueException{
		testLeague.registerTeam(testTeamOne);
		testLeague.registerTeam(testTeamTwo);
		testLeague.registerTeam(testTeamThree);
		testLeague.registerTeam(testTeamFour);
		testLeague.startNewSeason();
		testLeague.playMatch(teamNames[0], 3, teamNames[0], 2);
	}

	// test that an exception is thrown when attempting to return a top team from an empty league
	@Test (expected = LeagueException.class)
	public void testGetTopNoTeamException() throws LeagueException{
		testLeague.getTopTeam();
	}

    // test that an exception is thrown when attempting to return a top team from a league that is not full
	@Test (expected = LeagueException.class)
	public void testGetTopNotFullTeamException() throws LeagueException{
		testLeague.registerTeam(testTeamOne);
		testLeague.registerTeam(testTeamTwo);
		testLeague.getTopTeam();
	}

	// test that an exception is thrown when attempting to return a bottom team from an empty league
	@Test (expected = LeagueException.class)
	public void testGetBotNoTeamException() throws LeagueException{
		testLeague.getBottomTeam();
	}
	
    // test that an exception is thrown when attempting to return a bottom team from a league that is not full
	@Test (expected = LeagueException.class)
	public void testGetBotNotFullTeamException() throws LeagueException{
		testLeague.registerTeam(testTeamOne);
		testLeague.registerTeam(testTeamTwo);
		testLeague.getBottomTeam();
	}
	
}


