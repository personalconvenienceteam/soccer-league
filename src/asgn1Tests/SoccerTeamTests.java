package asgn1Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn1Exceptions.TeamException;
import asgn1SoccerCompetition.SoccerTeam;



/**
 * A set of JUnit tests for the asgn1SoccerCompetition.SoccerLeage class
 *
 * @author Alan Woodley
 *
 */
public class SoccerTeamTests {
	
	final String[] teamNames = {"TeamOne", "T1", "TeamTwo", "T2", "TeamThree", 
			  "T3", "TeamFour", "T4"}; 					  //String of Arrays for team names

	SoccerTeam testTeamOne, testTeamTwo, testTeamThree, testTeamFour, exceptionTestTeam; //Field names for test teams
	
	/**
	 * Construct teams for testing with names from teamNames Array
	 * 
	 * @throws TeamException
	 */
	@Before
	public void constructTestTeams() throws TeamException{
		testTeamOne = new SoccerTeam(teamNames[0],teamNames[1]);
		testTeamTwo = new SoccerTeam(teamNames[2],teamNames[3]);
		testTeamThree = new SoccerTeam(teamNames[4],teamNames[5]);
		testTeamFour = new SoccerTeam(teamNames[6],teamNames[7]);
	}
	
	/**
	 * Test constructor throws exception when both names are
	 * empty strings
	 * 
	 * @throws TeamException
	 */
	@Test(expected = TeamException.class)
	public void teamConstructorTest() throws TeamException{
			exceptionTestTeam = new SoccerTeam("","");
	}
	
	/**
	 * Test constructor throws exception when official name
	 * is empty string
	 * 
	 * @throws TeamException
	 */
	@Test(expected = TeamException.class)
	public void teamConstructorTestOff() throws TeamException{
			exceptionTestTeam = new SoccerTeam("","Test");
	}

	/**
	 * Test constructor throws exception when nick name is
	 * empty string
	 * 
	 * @throws TeamException
	 */
	@Test(expected = TeamException.class)
	public void teamConstructorTestNick() throws TeamException{
			exceptionTestTeam = new SoccerTeam("Test","");
	}

	/**
	 * Test Following set of tests test that names have
	 * been assigned correctly and return correctly
	 */
	@Test
	public void testTeamOneOfficial(){
		assertEquals(testTeamOne.getOfficialName(),teamNames[0]);
	}
	
	@Test
	public void testTeamOneNick(){
		assertEquals(testTeamOne.getNickName(),teamNames[1]);
	}
	
	@Test
	public void testTeamTwoOfficial(){
		assertEquals(testTeamTwo.getOfficialName(),teamNames[2]);
	}
	
	@Test
	public void testTeamTwoNick(){
		assertEquals(testTeamTwo.getNickName(),teamNames[3]);
	}
	
	@Test
	public void testTeamThreeOfficial(){
		assertEquals(testTeamThree.getOfficialName(),teamNames[4]);
	}
	
	@Test
	public void testTeamThreeNick(){
		assertEquals(testTeamThree.getNickName(),teamNames[5]);
	}
	@Test
	public void testTeamFourOfficial(){
		assertEquals(testTeamFour.getOfficialName(),teamNames[6]);
	}
	
	@Test
	public void testTeamFourNick(){
		assertEquals(testTeamFour.getNickName(),teamNames[7]);
	}
	
	/**
	 * Test team statistics are correctly initiated
	 */
	@Test
	public void testStatsGoals(){
		final int expected = 0;
		assertEquals(testTeamOne.getGoalsScoredSeason(), expected);
	}
	
	@Test
	public void testStatsConceded(){
		final int expected = 0;
		assertEquals(testTeamOne.getGoalsConcededSeason(), expected);
	}
	
	@Test
	public void testStatsWon(){
		final int expected = 0;
		assertEquals(testTeamOne.getMatchesWon(), expected);
	}
	
	@Test
	public void testStatsDraw(){
		final int expected = 0;
		assertEquals(testTeamOne.getMatchesDrawn(), expected);
	}
	
	@Test
	public void testStatsLoss(){
		final int expected = 0;
		assertEquals(testTeamOne.getMatchesLost(), expected);
	}
	
	@Test
	public void testStatsCompPoints(){
		final int expected = 0;
		assertEquals(testTeamOne.getCompetitionPoints(), expected);
	}
	
	@Test
	public void testEmptyForm(){
		final String results = "-----";
		assertEquals(testTeamOne.getFormString(), results);
	}
	
	/**
	 * Test matches play correctly and teams update
	 * First set tests exception handling
	 */
	@Test(expected = TeamException.class)
	public void testMatchPositiveExceptionF() throws TeamException{
		final int goalsFor = 30;
		final int goalsAgainst = 0;
	
		testTeamOne.playMatch(goalsFor, goalsAgainst);
	}

	@Test(expected = TeamException.class)
	public void testMatchPositiveNegativeF() throws TeamException{
		final int goalsFor = -1;
		final int goalsAgainst = 0;

		testTeamOne.playMatch(goalsFor, goalsAgainst);

	}

	@Test(expected = TeamException.class)
	public void testMatchPositiveExceptionA() throws TeamException{
		final int goalsFor = 0;
		final int goalsAgainst = 30;

		testTeamOne.playMatch(goalsFor, goalsAgainst);
	}
	
	@Test(expected = TeamException.class)
	public void testMatchNegativeExceptionA() throws TeamException{
		final int goalsFor = 0;
		final int goalsAgainst = -1;

		testTeamOne.playMatch(goalsFor, goalsAgainst);

	}

	/**
	 * Test matches play correctly and teams update
	 * Next set tests a single winning game
	 */
	@Test
	public void testMatchWin() throws TeamException{
		final int goalsFor = 4;
		final int goalsAgainst = 2;
		final int ExpectedWins = 1;
		testTeamOne.playMatch(goalsFor, goalsAgainst);
		assertEquals(testTeamOne.getMatchesWon(), ExpectedWins);
	}
	
	@Test
	public void testMatchWinNoLoss() throws TeamException{
		final int goalsFor = 4;
		final int goalsAgainst = 2;
		final int ExpectedLoss = 0;
		testTeamOne.playMatch(goalsFor, goalsAgainst);
		assertEquals(testTeamOne.getMatchesLost(), ExpectedLoss);
	}
	
	@Test
	public void testMatchWinNoDraw() throws TeamException{
		final int goalsFor = 4;
		final int goalsAgainst = 2;
		final int ExpectedDraw = 0;
		testTeamOne.playMatch(goalsFor, goalsAgainst);
		assertEquals(testTeamOne.getMatchesDrawn(), ExpectedDraw);
	}
	
	@Test
	public void testMatchWinGoals() throws TeamException{
		final int goalsFor = 4;
		final int goalsAgainst = 2;
		final int ExpectedGoals = 4;
		testTeamOne.playMatch(goalsFor, goalsAgainst);
		assertEquals(testTeamOne.getGoalsScoredSeason(), ExpectedGoals);
	}

	@Test
	public void testMatchWinConceded() throws TeamException{
		final int goalsFor = 4;
		final int goalsAgainst = 2;
		final int ExpectedConceded = 2;
		testTeamOne.playMatch(goalsFor, goalsAgainst);
		assertEquals(testTeamOne.getGoalsConcededSeason(), ExpectedConceded);
	}
	
	@Test
	public void testMatchWinCompPoints() throws TeamException{
		final int goalsFor = 4;
		final int goalsAgainst = 2;
		final int ExpectedPoints = 3;
		testTeamOne.playMatch(goalsFor, goalsAgainst);
		assertEquals(testTeamOne.getCompetitionPoints(), ExpectedPoints);
	}
	
	@Test
	public void testOneWinForm() throws TeamException{
		final String results = "W----";
		final int goalsFor = 4;
		final int goalsAgainst = 2;
		testTeamOne.playMatch(goalsFor, goalsAgainst);
		assertEquals(testTeamOne.getFormString(), results);
	}

	/**
	 * Test matches play correctly and teams update
	 * Next set for a single loosing game
	 */
	@Test
	public void testMatchLoss() throws TeamException{
		final int goalsFor = 2;
		final int goalsAgainst = 4;
		final int ExpectedWins = 0;
		testTeamOne.playMatch(goalsFor, goalsAgainst);
		assertEquals(testTeamOne.getMatchesWon(), ExpectedWins);
	}
	
	@Test
	public void testMatchLossNoWin() throws TeamException{
		final int goalsFor = 2;
		final int goalsAgainst = 4;
		final int ExpectedLoss = 1;
		testTeamOne.playMatch(goalsFor, goalsAgainst);
		assertEquals(testTeamOne.getMatchesLost(), ExpectedLoss);
	}
	
	@Test
	public void testMatchLossNoDraw() throws TeamException{
		final int goalsFor = 2;
		final int goalsAgainst = 4;
		final int ExpectedDraw = 0;
		testTeamOne.playMatch(goalsFor, goalsAgainst);
		assertEquals(testTeamOne.getMatchesDrawn(), ExpectedDraw);
	}
	
	@Test
	public void testMatchLossGoals() throws TeamException{
		final int goalsFor = 2;
		final int goalsAgainst = 4;
		final int ExpectedGoals = 2;
		testTeamOne.playMatch(goalsFor, goalsAgainst);
		assertEquals(testTeamOne.getGoalsScoredSeason(), ExpectedGoals);
	}

	@Test
	public void testMatchLossConceded() throws TeamException{
		final int goalsFor = 2;
		final int goalsAgainst = 4;
		final int ExpectedConceded = 4;
		testTeamOne.playMatch(goalsFor, goalsAgainst);
		assertEquals(testTeamOne.getGoalsConcededSeason(), ExpectedConceded);
	}
	
	@Test
	public void testMatchLossCompPoints() throws TeamException{
		final int goalsFor = 2;
		final int goalsAgainst = 4;
		final int ExpectedPoints = 0;
		testTeamOne.playMatch(goalsFor, goalsAgainst);
		assertEquals(testTeamOne.getCompetitionPoints(), ExpectedPoints);
	}
	
	@Test
	public void testOneLossForm() throws TeamException{
		final String results = "L----";
		final int goalsFor = 2;
		final int goalsAgainst = 4;
		testTeamOne.playMatch(goalsFor, goalsAgainst);
		assertEquals(testTeamOne.getFormString(), results);
	}


	/**
	 * Test matches play correctly and teams update
	 * Next set for multiple games
	 */
	@Test
	public void testMatchMultiGameWins() throws TeamException{
		final int NumGames = 5;
		final int[] goalsFor = {2, 15, 5, 6, 9};
		final int[] goalsAgainst = {4, 6, 5, 3, 2};
		final int ExpectedWins = 3;
		for (int game=0; game<NumGames; game++){
			testTeamOne.playMatch(goalsFor[game], goalsAgainst[game]);
		}
		assertEquals(testTeamOne.getMatchesWon(), ExpectedWins);
	}
	
	@Test
	public void testMatchMultiGameLosses() throws TeamException{
		final int NumGames = 5;
		final int[] goalsFor = {2, 15, 5, 6, 9};
		final int[] goalsAgainst = {4, 6, 5, 3, 2};
		final int ExpectedLoss = 1;
		for (int game=0; game<NumGames; game++){
			testTeamOne.playMatch(goalsFor[game], goalsAgainst[game]);
		}
		assertEquals(testTeamOne.getMatchesLost(), ExpectedLoss);
	}
	
	@Test
	public void testMatchMultiGameDraw() throws TeamException{
		final int NumGames = 5;
		final int[] goalsFor = {2, 15, 5, 6, 9};
		final int[] goalsAgainst = {4, 6, 5, 3, 2};
		final int ExpectedDraw = 1;
		for (int game=0; game<NumGames; game++){
			testTeamOne.playMatch(goalsFor[game], goalsAgainst[game]);
		}
		assertEquals(testTeamOne.getMatchesDrawn(), ExpectedDraw);
	}
	
	@Test
	public void testMatchMultiGameGoals() throws TeamException{
		final int NumGames = 5;
		final int[] goalsFor = {2, 15, 5, 6, 9};
		final int[] goalsAgainst = {4, 6, 5, 3, 2};
		final int ExpectedGoals = 37;
		for (int game=0; game<NumGames; game++){
			testTeamOne.playMatch(goalsFor[game], goalsAgainst[game]);
		}
		assertEquals(testTeamOne.getGoalsScoredSeason(), ExpectedGoals);
	}

	@Test
	public void testMatchMultiGameConceded() throws TeamException{
		final int NumGames = 5;
		final int[] goalsFor = {2, 15, 5, 6, 9};
		final int[] goalsAgainst = {4, 6, 5, 3, 2};
		final int ExpectedConceded = 20;
		for (int game=0; game<NumGames; game++){
			testTeamOne.playMatch(goalsFor[game], goalsAgainst[game]);
		}
		assertEquals(testTeamOne.getGoalsConcededSeason(), ExpectedConceded);
	}
	
	@Test
	public void testMatchMultiGameCompPoints() throws TeamException{
		final int NumGames = 5;
		final int[] goalsFor = {2, 15, 5, 6, 9};
		final int[] goalsAgainst = {4, 6, 5, 3, 2};
		final int ExpectedPoints = 10;
		for (int game=0; game<NumGames; game++){
			testTeamOne.playMatch(goalsFor[game], goalsAgainst[game]);
		}
		assertEquals(testTeamOne.getCompetitionPoints(), ExpectedPoints);
	}
	
	@Test
	public void testMultiGameForm() throws TeamException{
		final String results = "LWDWW";
		final int NumGames = 5;
		final int[] goalsFor = {2, 15, 5, 6, 9};
		final int[] goalsAgainst = {4, 6, 5, 3, 2};
		for (int game=0; game<NumGames; game++){
		testTeamOne.playMatch(goalsFor[game], goalsAgainst[game]);
		}
		assertEquals(testTeamOne.getFormString(), results);
	}
	
	@Test
	public void testMatchFullForm() throws TeamException{
		final String results = "WWDWL";
		final int NumGames = 9;
		final int[] goalsFor = {2, 15, 5, 6, 9, 7, 1, 6, 4};
		final int[] goalsAgainst = {4, 6, 5, 3, 2, 0, 1, 3, 7};
		for (int game=0; game<NumGames; game++){
			testTeamOne.playMatch(goalsFor[game], goalsAgainst[game]);
		}
		assertEquals(testTeamOne.getFormString(), results);
	}
	
	/**
	 * Test for the compareTo() method
	 */
	@Test
	public void testCompareToGreaterResult() throws TeamException{
		final int NumGames = 9;
		final int[] goalsFor =     {2, 15, 5, 6, 9, 7, 1, 6, 4};
		final int[] goalsAgainst = {4, 6, 5, 3, 2, 0, 1, 3, 7};
		final int ExpectedCompareResult = -9;
		for (int game=0; game<NumGames; game++){
			testTeamOne.playMatch(goalsFor[game], goalsAgainst[game]);
			testTeamTwo.playMatch(goalsAgainst[game], goalsFor[game]);
		}
		assertEquals(testTeamOne.compareTo(testTeamTwo), ExpectedCompareResult);
	}
	
	@Test
	public void testCompareToLowerResult() throws TeamException{
		final int NumGames = 9;
		final int[] goalsFor =     {2, 15, 5, 6, 9, 7, 1, 6, 4};
		final int[] goalsAgainst = {4, 6, 5, 3, 2, 0, 1, 3, 7};
		final int ExpectedCompareResult = 9;
		for (int game=0; game<NumGames; game++){
			testTeamOne.playMatch(goalsFor[game], goalsAgainst[game]);
			testTeamTwo.playMatch(goalsAgainst[game], goalsFor[game]);
		}
		assertEquals(testTeamTwo.compareTo(testTeamOne), ExpectedCompareResult);
	}

	@Test
	public void testCompareToSameResult() throws TeamException{
		final int NumGames = 9;
		final int[] goalsFor =     {2, 15, 5, 6, 9, 7, 1, 6, 4};
		final int[] goalsAgainst = {4, 6, 5, 3, 2, 0, 1, 3, 7};
		final int ExpectedCompareResult = 0;
		for (int game=0; game<NumGames; game++){
			testTeamOne.playMatch(goalsFor[game], goalsAgainst[game]);
		}
		assertEquals(testTeamOne.compareTo(testTeamOne), ExpectedCompareResult);
	}
	
	@Test
	public void testCompareToAlphaResult() throws TeamException{
		final int NumGames = 9;
		final int[] goalsFor =     {2, 15, 5, 6, 9, 7, 1, 6, 4};
		final int[] goalsAgainst = {4, 6, 5, 3, 2, 0, 1, 3, 7};
		final int ExpectedCompareResult = 5;
		for (int game=0; game<NumGames; game++){
			testTeamOne.playMatch(goalsFor[game], goalsAgainst[game]);
			testTeamTwo.playMatch(goalsFor[game], goalsAgainst[game]);
		}
		assertEquals(testTeamTwo.compareTo(testTeamOne), ExpectedCompareResult);
	}
	
	@Test
	public void testResetGoals() throws TeamException{
		final int NumGames = 9;
		final int[] goalsFor =     {2, 15, 5, 6, 9, 7, 1, 6, 4};
		final int[] goalsAgainst = {4, 6, 5, 3, 2, 0, 1, 3, 7};
		final int ExpectedResetGoals = 0;
		for (int game=0; game<NumGames; game++){
			testTeamOne.playMatch(goalsFor[game], goalsAgainst[game]);
		}
		testTeamOne.resetStats();
		assertEquals(testTeamOne.getGoalsScoredSeason(), ExpectedResetGoals);
	}
	
	@Test
	public void testResetConceded() throws TeamException{
		final int NumGames = 9;
		final int[] goalsFor =     {2, 15, 5, 6, 9, 7, 1, 6, 4};
		final int[] goalsAgainst = {4, 6, 5, 3, 2, 0, 1, 3, 7};
		final int ExpectedResetResult = 0;
		for (int game=0; game<NumGames; game++){
			testTeamOne.playMatch(goalsFor[game], goalsAgainst[game]);
		}
		testTeamOne.resetStats();
		assertEquals(testTeamOne.getGoalsConcededSeason(), ExpectedResetResult);
	}

	@Test
	public void testResetmatchesWon() throws TeamException{
		final int NumGames = 9;
		final int[] goalsFor =     {2, 15, 5, 6, 9, 7, 1, 6, 4};
		final int[] goalsAgainst = {4, 6, 5, 3, 2, 0, 1, 3, 7};
		final int ExpectedResetResult = 0;
		for (int game=0; game<NumGames; game++){
			testTeamOne.playMatch(goalsFor[game], goalsAgainst[game]);
		}
		testTeamOne.resetStats();
		assertEquals(testTeamOne.getMatchesWon(), ExpectedResetResult);
	}

	@Test
	public void testResetmatchesLost() throws TeamException{
		final int NumGames = 9;
		final int[] goalsFor =     {2, 15, 5, 6, 9, 7, 1, 6, 4};
		final int[] goalsAgainst = {4, 6, 5, 3, 2, 0, 1, 3, 7};
		final int ExpectedResetResult = 0;
		for (int game=0; game<NumGames; game++){
			testTeamOne.playMatch(goalsFor[game], goalsAgainst[game]);
		}
		testTeamOne.resetStats();
		assertEquals(testTeamOne.getMatchesLost(), ExpectedResetResult);
	}

	@Test
	public void testResetmatchesDrawn() throws TeamException{
		final int NumGames = 9;
		final int[] goalsFor =     {2, 15, 5, 6, 9, 7, 1, 6, 4};
		final int[] goalsAgainst = {4, 6, 5, 3, 2, 0, 1, 3, 7};
		final int ExpectedResetResult = 0;
		for (int game=0; game<NumGames; game++){
			testTeamOne.playMatch(goalsFor[game], goalsAgainst[game]);
		}
		testTeamOne.resetStats();
		assertEquals(testTeamOne.getMatchesDrawn(), ExpectedResetResult);
	}

	@Test
	public void testResetPoints() throws TeamException{
		final int NumGames = 9;
		final int[] goalsFor =     {2, 15, 5, 6, 9, 7, 1, 6, 4};
		final int[] goalsAgainst = {4, 6, 5, 3, 2, 0, 1, 3, 7};
		final int ExpectedResetResult = 0;
		for (int game=0; game<NumGames; game++){
			testTeamOne.playMatch(goalsFor[game], goalsAgainst[game]);
		}
		testTeamOne.resetStats();
		assertEquals(testTeamOne.getCompetitionPoints(), ExpectedResetResult);
	}

	@Test
	public void testResetForm() throws TeamException{
		final int NumGames = 9;
		final int[] goalsFor =     {2, 15, 5, 6, 9, 7, 1, 6, 4};
		final int[] goalsAgainst = {4, 6, 5, 3, 2, 0, 1, 3, 7};
		final String ExpectedResetResult = "-----";
		for (int game=0; game<NumGames; game++){
			testTeamOne.playMatch(goalsFor[game], goalsAgainst[game]);
		}
		testTeamOne.resetStats();
		assertEquals(testTeamOne.getFormString(), ExpectedResetResult);
	}

}
