package asgn1Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn1SoccerCompetition.SportsTeamForm;
import asgn1SportsUtils.WLD;

/**
 * A set of JUnit tests for the asgn1SoccerCompetition.SoccerTeamForm class
 *
 * @author Roderick Lenz
 *
 */
public class SportsTeamFormTests {

	SportsTeamForm testTeamForm; // Sample form used for testing

	/**
	 * Construct a SportsTeamForm to use in tests.  
	 */
	@Before
	public void constructSportsTeamForm(){
		testTeamForm = new SportsTeamForm();
	}
	
	/**
	 * Check testTeamForm is empty 
	 */
	@Test
	public void testFormEmpty(){
		final String testResult = "-----"; 
		assertEquals(testTeamForm.toString(), testResult);
	}
	
	/**
	 * Check Number of games is initial set to zero 
	 */
	@Test
	public void testInitialNumGames(){
		final int expectedNumGames = 0;
		assertEquals(testTeamForm.getNumGames(), expectedNumGames);
	}
	
	
	/**
	 * Test a single result can be added to the form and returned as a string.
	 */
	@Test
	public void singleTestSportsTeamForm(){
		final String testResult = "W----";
		testTeamForm.addResultToForm(WLD.WIN);
		assertEquals(testTeamForm.toString(), testResult);
	}
	
	/**
	 * Test two results can be added to the form and returned as a string.
	 */
	@Test
	public void twoGameTestSportsTeamForm(){
		final String testResult = "WD---";
		testTeamForm.addResultToForm(WLD.WIN);
		testTeamForm.addResultToForm(WLD.DRAW);
		assertEquals(testTeamForm.toString(), testResult);
	}

	/**
	 * Test three results can be added to the form and returned as a string.
	 */
	@Test
	public void threeGameTestSportsTeamForm(){
		final String testResult = "WDW--";
		testTeamForm.addResultToForm(WLD.WIN);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.WIN);
		assertEquals(testTeamForm.toString(), testResult);
	}
	
	/**
	 * Test four results can be added to the form and returned as a string.
	 */
	@Test
	public void fourGameTestSportsTeamForm(){
		final String testResult = "WDWL-";
		testTeamForm.addResultToForm(WLD.WIN);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.WIN);
		testTeamForm.addResultToForm(WLD.LOSS);
		assertEquals(testTeamForm.toString(), testResult);
	}
	
	/**
	 * 
	 * Test 5 results can be added to the form and the
	 * string returned in the appropriate format.
	 * 
	 */
	@Test
	public void orderTestSportsTeamForm(){
		final String testResult = "WDLWD";
		testTeamForm.addResultToForm(WLD.WIN);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.LOSS);
		testTeamForm.addResultToForm(WLD.WIN);
		testTeamForm.addResultToForm(WLD.DRAW);
		assertEquals(testTeamForm.toString(), testResult);
	}
	
	/**
	 * 
	 * Test more results to ensure form only stores 5
	 * most recent games
	 * 
	 */
	@Test
	public void testFullSportsTeamForm(){
		final String testResult = "DDDDL";
		testTeamForm.addResultToForm(WLD.WIN);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.LOSS);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.LOSS);
		assertEquals(testTeamForm.toString(), testResult);
	}
	
	
	/**
	 * 
	 *  Test that number of games played increments correctly
	 *  
	 */
	@Test
	public void testNumberOfGame(){
		final int numberOfGames = 8;
		testTeamForm.addResultToForm(WLD.WIN);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.LOSS);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.LOSS);
		assertEquals(testTeamForm.getNumGames(), numberOfGames);
	}
	
	
	/**
	 * 
	 * Test the form resets results correctly
	 * 
	 */
	@Test
	public void testResetResults(){
		final String testResult = "-----";
		testTeamForm.addResultToForm(WLD.WIN);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.LOSS);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.LOSS);
		testTeamForm.resetForm();
		assertEquals(testTeamForm.toString(), testResult);
	}
	
	/**
	 * 
	 * Test the form resets number of games correctly
	 * 
	 */
	@Test
	public void testResetNumGames(){
		final int expectedNumber = 0;
		testTeamForm.addResultToForm(WLD.WIN);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.LOSS);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.DRAW);
		testTeamForm.addResultToForm(WLD.LOSS);
		testTeamForm.resetForm();
		assertEquals(testTeamForm.getNumGames(), expectedNumber);
	}
}
